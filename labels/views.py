from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from .models import *

# Create your views here.
DUMMY = ["hiking", "swimming", "skiing", "surfing"]

def require_is_admin(view):
    ERRMSG = 'You must be an admin to view this page'
    def wrapper(*args, **kwargs):
        request = args[0]
        try:
            if not request.user.profile.is_admin:
                return HttpResponse(ERRMSG, status=401)
            else:
                return view(*args, **kwargs)
        except User.DoesNotExist:
            return HttpResponse(ERRMSG, status=401)
        except AttributeError:
            return HttpResponse(ERRMSG, status=401)            
    return wrapper

@require_is_admin
def manage(request):
    context = {'labels': [label.name for label in ActivityLabel.objects.all()]}
    return render(request, 'labels/manage.html', context)

@require_is_admin
def add(request):
    if request.method == 'POST':
        new_label_name = request.POST['label-name']
        if ActivityLabel.objects.filter(name=new_label_name).exists():
            messages.error(request, 'Label already exists')
        else:
            ActivityLabel(name=new_label_name).save()
            messages.success(request, 'Label added')
    context = {}
    return render(request, 'labels/add.html', context)

@require_is_admin
def edit(request):
    context = {}
    if request.method == 'POST':
        old_label_name = request.POST['label-name']
        new_label_name = request.POST['new-label-name']
        if ActivityLabel.objects.filter(name=old_label_name).exists():
            if ActivityLabel.objects.filter(name=new_label_name).exists():
                messages.error(request, f'Label "{new_label_name}" already exists')
                context['label_name'] = old_label_name
                context['new_label_name'] = new_label_name
            else:
                curr_label = ActivityLabel.objects.get(name=old_label_name)
                curr_label.name = new_label_name
                curr_label.save(force_update=True)
                messages.success(request, f'Label "{old_label_name}" changed to "{new_label_name}"')
        else:
            messages.error(request, f'Label "{old_label_name}" does not exist')
            context['label_name'] = old_label_name
            context['new_label_name'] = new_label_name
    return render(request, 'labels/edit.html', context)

@require_is_admin
def delete(request):
    if request.method == 'POST':
        label_name = request.POST['label-name']
        to_delete = ActivityLabel.objects.filter(name=label_name)
        if to_delete.exists():
            to_delete.delete()
            messages.success(request, 'Label deleted.')
        else:
            messages.error(request, 'Label does not exist')
    context = {}
    return render(request, 'labels/delete.html', context)
