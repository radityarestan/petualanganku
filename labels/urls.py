from django.urls import path
from . import views
app_name = 'labels'

urlpatterns = [
    path('', views.manage, name='manage'),
    path('add/', views.add, name='add'),
    path('edit/', views.edit, name='edit'),
    path('delete/', views.delete, name='delete'),
]
