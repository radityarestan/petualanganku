from django.db import models
from blog.models import Blog
# Create your models here.

class ActivityLabel(models.Model):
    name = models.TextField(blank=False, unique=True)
    blogs = models.ManyToManyField(Blog, related_name="activity_labels", blank=True)
