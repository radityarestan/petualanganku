from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse
from django.db.models import base, Prefetch
from django.shortcuts import render, get_object_or_404, redirect
from django.http import QueryDict, HttpResponseNotFound

import json

from .models import *
from labels.models import ActivityLabel
from .utils import decode_base64_file

# Create your views here.

@login_required
def create(request):
    blog = Blog.objects.create(author=request.user)
    return redirect('/blog/write/' + str(blog.id))

@login_required
@require_http_methods(['GET', 'POST'])
def write(request, id):
    blog = Blog.objects.filter(id=id).prefetch_related(Prefetch('sections', queryset=ContentSection.objects.order_by('order'))).get()

    if blog.author.username != request.user.username:
        return HttpResponse('You need to be the author to edit a blog')

    if request.method == 'GET':
        return render(request, 'blog/write_blog.html', context={'blog': blog})

    elif request.method == 'POST':
        blog.title = request.POST.get('title')

        if 'base64' in request.POST.get('thumbnail'):
            blog.thumbnail = decode_base64_file(request.POST.get('thumbnail'))

        blog.save()

        sections = json.loads(request.POST.get('sections'))

        # Update ContentSection
        for order in sections:
            section = sections[order]
            
            if section['type'] == 'text':
                if section['id'] == '0':
                    section_model = ContentSection(blog=blog, order=int(order), text_content=section['content'])
                    section_model.save()

                else:
                    ContentSection.objects.filter(id=int(section['id'])).update(order=int(order), text_content=section['content'])

            else:
                image = decode_base64_file(section['content'])

                if section['id'] == '0':
                    section_model = ContentSection(blog=blog, order=int(order), image_content=image, caption_content=section['caption'])
                    section_model.save()

                else:
                    ContentSection.objects.filter(id=int(section['id'])).update(order=int(order), caption_content=section['caption'])

        return HttpResponse('Success')

@login_required
@require_http_methods(['GET'])
def delete_blog_section(request, section_id):
    section = ContentSection.objects.filter(id=section_id).get()

    if section.blog.author.username == request.user.username:
        section.delete()

        return HttpResponse('Success')

@login_required
@require_http_methods(['GET'])
def publish_blog(request, blog_id):
    blog = Blog.objects.filter(id=blog_id).select_related('author').get()

    if blog.author.username == request.user.username:
        blog.is_published = True
        blog.save()

        return HttpResponse('Success')

@login_required
@require_http_methods(['GET'])
def unpublish_blog(request, blog_id):
    blog = Blog.objects.filter(id=blog_id).select_related('author').get()

    if blog.author.username == request.user.username:
        blog.is_published = False
        blog.save()

        return HttpResponse('Success')

@login_required
@require_http_methods(['GET'])
def blog_list(request):
    blogs = Blog.objects.filter(author=request.user).prefetch_related(Prefetch('sections', queryset=ContentSection.objects.order_by('order'))).all()
    return render(request, 'blog/list_blog.html', context={'blogs': blogs})

@login_required
def create_area_label(request, id):
    if request.method == "POST":
        province = request.POST.get('province')
        province = province.split('-')
        city = request.POST.get('city')
        city = city.split('-')

        blog = Blog.objects.get(id=id)

        if blog.author.username != request.user.username:
            return HttpResponse('You need to be the author to edit a blog')
        
        try:
            area = blog.blog_area_label.all().get(province=province[1], city=city[1])
            if area:
                message = "label existed"
        except Area.DoesNotExist:
            message = "label created"
            Area.objects.create(province=province[1], city=city[1], blog_labeled=blog)

        list_area_label = { area.id : str(area) for area in blog.blog_area_label.all()}

        dict_data_area = json.loads(json.dumps({'list_area_label' : [list_area_label], 'message':message}))
        
        return JsonResponse(dict_data_area, safe=False)    

@login_required
def delete_area_label(request, id, area_id):
    deleted, message = None, None
    blog = Blog.objects.get(id=id)
    area_label = blog.blog_area_label.all()

    if blog:
        if blog.author.username != request.user.username:
            return HttpResponse('You need to be the author to edit a blog')

        if area_label:
            deleted = area_label.get(id=area_id).delete()
            if deleted:
                message = "Label has been deleted!"
            else:
                message = "Fail deleting label, please try again!"

        else:
            message = "No label can be found!"
    else:
        message = "No blog can be found!"
    
    dict_delete_status = json.loads(json.dumps({'status_delete' : message}))

    return JsonResponse(dict_delete_status, safe=False)

@login_required
def create_activity_label(request, id):
    if request.method == "POST":
        activity = request.POST.get('activity')
        activity = activity.split('-')

        blog = Blog.objects.get(id=id)
        
        if blog.author.username != request.user.username:
            return HttpResponse('You need to be the author to edit a blog')

        try:
            act = blog.activity_labels.all().get(name=activity[1])    
            if act:
                message = "label existed"
        except ActivityLabel.DoesNotExist:
            message = "label created"
            act = ActivityLabel.objects.get(id=activity[0])
            blog.activity_labels.add(act)
            blog.save()

        list_act_label = { activity.id : activity.name for activity in blog.activity_labels.all()}

        dict_data_act = json.loads(json.dumps({'list_activity_label' : [list_act_label], 'message':message}))
        
    else:
        list_all_act_label = { activity.id : activity.name for activity in ActivityLabel.objects.all() }
    
        dict_data_act = json.loads(json.dumps({'list_all_act_label' : [list_all_act_label]}))

    return JsonResponse(dict_data_act, safe=False)

@login_required
def delete_activity_label(request, id, activity_id):
    message = None
    blog = Blog.objects.get(id=id)
    act_label = ActivityLabel.objects.get(id=activity_id)

    if blog:
        if blog.author.username != request.user.username:
            return HttpResponse('You need to be the author to edit a blog')
            
        if act_label:
            blog.activity_labels.remove(act_label)
            blog.save()
            
            try:
                blog.activity_labels.all().get(id=activity_id)   
            except ActivityLabel.DoesNotExist:
                message = "Label has been deleted!"
        
        else:
            message = "No label can be found!"
    else:
        message = "No blog can be found!"
    
    dict_delete_status = json.loads(json.dumps({'status_delete' : message}))  

    return JsonResponse(dict_delete_status, safe=False)

def detail(request, blog_id):
    blog = Blog.objects.filter(id=blog_id).prefetch_related(Prefetch('sections', queryset=ContentSection.objects.order_by('order'))).get()

    if blog != None and (blog.is_published or request.user.username == blog.author.username):
        all_comments = Comment.objects.all().order_by('-timestamp')
        comments = []
        for base_comment in all_comments:
            if base_comment.blog == blog and base_comment.replyTo == None:
                base_replies_pair = []
                base_replies_pair.append(base_comment)
                base_comment_replies = []
                for reply_comment in all_comments:
                    if reply_comment.replyTo == base_comment:
                        base_comment_replies.append(reply_comment)
                base_replies_pair.append(base_comment_replies)
                comments.append(base_replies_pair)

        context = {
            "blog": blog,
            "comments": comments
        }
        return render(request, 'blog/blog_detail.html', context)

    else:
        return HttpResponseNotFound('Page doesn`t exists')

def create_comment(request, blog_id):
    if request.is_ajax and request.method == "POST" and request.user.is_authenticated:
        blog = get_object_or_404(Blog, id = blog_id)
        commentator = request.user
        new_comment = Comment(content=request.POST.get('newCommentContent', None) , blog=blog, commentator=commentator)
        new_comment.save()
        return JsonResponse({'blog_id' : blog.id}, status = 201)

def reply_comment(request, comment_id):
    if request.is_ajax and request.method == "POST" and request.user.is_authenticated:
        base_comment = get_object_or_404(Comment, id = comment_id)
        base_comment_blog = base_comment.blog
        commentator = request.user
        new_comment = Comment(content=request.POST.get('newCommentContent', None) , blog=base_comment_blog, commentator=commentator, replyTo=base_comment)
        new_comment.save()
        return JsonResponse({'blog_id' : base_comment_blog.id}, status = 201)

def edit_comment(request, comment_id):
    if request.is_ajax and request.method == "PUT" and request.user.is_authenticated:
        comment = get_object_or_404(Comment, id = comment_id)
        comment_blog = comment.blog
        put = QueryDict(request.body)
        new_comment_content = put.get('newCommentContent')
        if(comment.content == new_comment_content):
             return JsonResponse({}, status = 400)
        comment.content = new_comment_content
        comment.save()
        return JsonResponse({'blog_id' : comment_blog.id}, status = 201)

def delete_comment(request, comment_id):
    if request.is_ajax and request.method == "DELETE" and request.user.is_authenticated:
        comment = get_object_or_404(Comment, id = comment_id)
        comment_blog = comment.blog
        comment.delete()
        return JsonResponse({'blog_id' : comment_blog.id}, status = 200)

def report(request, blog_id):
    if request.method == "POST" and request.user.is_authenticated:
        reported_blog = get_object_or_404(Blog, id = blog_id)
        report_type = request.POST.get('report-type', None)
        report_description = request.POST.get('report-description', None)
        new_report = Report(blog=reported_blog, reportType=report_type, reportDescription=report_description)
        new_report.save()
        return HttpResponseRedirect(f'/blog/{blog_id}')