from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Blog(models.Model):
    author = models.ForeignKey(User, blank=False, on_delete=models.CASCADE, related_name='blogs')
    title = models.TextField(blank=True)
    thumbnail = models.ImageField(blank=True)
    is_published = models.BooleanField(blank=False, default=False)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

class ContentSection(models.Model):
    blog = models.ForeignKey(Blog, blank=False, on_delete=models.CASCADE, related_name='sections')
    order = models.IntegerField()
    text_content = models.TextField(blank=True)
    image_content = models.ImageField(blank=True)
    caption_content = models.TextField(blank=True)
    
class Area(models.Model):
    province = models.CharField(max_length=30)
    city = models.CharField(max_length=30)
    blog_labeled = models.ForeignKey(Blog, related_name="blog_area_label", on_delete=models.CASCADE, null=True)

    def __str__(self):
        return "{}, {}".format(self.province, self.city)

    def getLabel(self):
        return "{}, {}".format(self.province, self.city)

class Comment(models.Model):
    commentator = models.ForeignKey(User, blank=False, on_delete=models.CASCADE, related_name="userComments")
    blog = models.ForeignKey(Blog, blank=False, on_delete=models.CASCADE, related_name="comments")
    replyTo = models.ForeignKey("Comment", blank=True, null=True, on_delete=models.CASCADE, related_name="repliesToThisComment")
    content = models.TextField(blank=True)
    timestamp = models.DateTimeField(auto_now=True)

class Report(models.Model):
    blog = models.ForeignKey(Blog, blank=False, on_delete=models.CASCADE, related_name="report")
    reportType = models.TextField()
    reportDescription = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True) 
