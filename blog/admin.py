from django.contrib import admin
from .models import Blog, Comment, Report, Area
# Register your models here.

admin.site.register(Blog)
admin.site.register(Comment)
admin.site.register(Report)
admin.site.register(Area)