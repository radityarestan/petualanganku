$(document).ready(function() {

    // Utility

    function format(fmt, ...args) {
        if (!fmt.match(/^(?:(?:(?:[^{}]|(?:\{\{)|(?:\}\}))+)|(?:\{[0-9]+\}))+$/)) {
            throw new Error('invalid format string.');
        }
        return fmt.replace(/((?:[^{}]|(?:\{\{)|(?:\}\}))+)|(?:\{([0-9]+)\})/g, (m, str, index) => {
            if (str) {
                return str.replace(/(?:{{)|(?:}})/g, m => m[0]);
            } else {
                if (index >= args.length) {
                    throw new Error('argument index is out of range in format');
                }
                return args[index];
            }
        });
    }

    // Title sync

    var titleForm = document.getElementById('info-form-title');
    var titleEditor = $('#content-editor-title');
    titleForm.value = titleEditor.text();

    document.getElementById('content-editor-title').addEventListener('input', function() {
        titleForm.value = titleEditor.text();
    }, false);

    // Thumbnail update

    var THUMBNAIL_PREVIEW = `
    <img class="info-form-thumbnail-preview" src="{0}"/>
    `

    var thumbnail = $('.info-form-image-input');

    function changeThumbnail(url) {
        thumbnail.html(format(THUMBNAIL_PREVIEW, url));
    }
    
    function getBase64(file, callback) {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => callback(reader.result);
    }

    $('#info-form-thumbnail').on('change', function(e) {
        getBase64(e.target.files[0], changeThumbnail);
    });

    // Add and remove section

    var BLANK_TEXT_FORM = `
    <div class="editor editor-p">
        <input name="section-id" type="hidden" value="0"/>
        <button class="button-remove-section">
            <i class="fa fa-minus"></i>
        </button>
        <p class="form-text form-content" contenteditable="true" placeholder="Write something..." spellcheck="false"></p>
    </div>
    `

    var IMAGE_SECTION = `
    <div class="editor editor-image">
        <input name="section-id" type="hidden" value="0"/>
        <button class="button-remove-section">
            <i class="fa fa-minus"></i>
        </button>
        <img class="image-content" src="{0}"/>
        <p class="form-text form-caption" contenteditable="true" placeholder="Image caption (optional)" spellcheck="false"></p>
    </div>
    `

    $('button.button-add-section-text').click(function() {
        $(this).parent().before(BLANK_TEXT_FORM)
    });

    function addImageSection(url) {
        $('#form-image').parent().before(format(IMAGE_SECTION, url));
    }

    $('#form-image').on('change', function(e) {
        getBase64(e.target.files[0], addImageSection);
        $(this).val('');
    });

    $('#content-editor').on('click', '.button-remove-section', function() {
        var sectionId = $(this).parent().find('input[name=section-id]').val();

        if (sectionId != '0') {
            $.ajax({
                type: 'GET',
                url: '/blog/section/delete/' + sectionId,
                xhrFields: {
                    withCredentials: true,
                },
                success: function(response) {
                    if (response != "Success") {
                        alert('An error occured while deleting section, changes might not be saved correctly.');
                    }
                }
            });
        }

        $(this).parent().remove();
    });

    document.getElementById('content-editor').addEventListener('keypress', (e) => {
        if (e.keyCode == 13) {
            e.preventDefault();
        }
    });

    function setEndOfContenteditable(contentEditableElement) {
        // Source: https://stackoverflow.com/a/3866442
        var range,selection;

        if(document.createRange) {
            range = document.createRange();
            range.selectNodeContents(contentEditableElement);
            range.collapse(false);
            selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);
        } else if(document.selection) { 
            range = document.body.createTextRange();
            range.moveToElementText(contentEditableElement);
            range.collapse(false);
            range.select();
        }
    }

    $(document).on('keyup', '.form-text', function(e) {
        // Backspace
        if (e.keyCode == 8) {
            if ($(this).is(':empty') && $(this).hasClass('form-content')) {
                var prev = $(this).parent().prev().find('.form-text');
                setEndOfContenteditable(prev.get(0));

                $(this).parent().remove();
            }
        }
        // Enter
        else if (e.keyCode == 13) {
            $(this).parent().after(BLANK_TEXT_FORM);
            var next = $(this).parent().next().find('.form-text');
            next.focus();
        }
    });

    // Change selected status for editor

    var prevButton = null;

    $('#content-editor').on('focus', '.editor', function(e) {
        if (prevButton != null) {
            prevButton.css('visibility', 'hidden');
        }

        prevButton = $(this).find('.button-remove-section');
        prevButton.css('visibility', 'visible');
        $(this).addClass('editor-focus');
    });

    $('#content-editor').on('blur', '.editor', function(e) {
        $(this).removeClass('editor-focus');
    });

    // Text editing

    var getSelected = function() {
        var t = '';

        if (window.getSelection) {
            t = window.getSelection();
        } else if (document.getSelection) {
            t = document.getSelection();
        } else if (document.selection) {
            t = document.selection.createRange().text;
        }
        
        return t;
    }

    var toolsWidth = $('#tools').width();

    $(document).bind('mouseup', function() {
        var selectedText = getSelected();
        var bound = selectedText.getRangeAt(0).getBoundingClientRect();

        if(selectedText != ''){
            if (Boolean($(selectedText.anchorNode).closest('.form-text'))) {
                $('#tools').css({
                    'left': (bound['left'] + bound['right']) / 2 - toolsWidth / 2,
                    'top' : bound['top'] - 40,
                }).fadeIn(200);
            }
        } else {
            $('#tools').fadeOut(200);
        }
    });

    $('.button-bold').click(function() {
        document.execCommand('bold');
    });

    $('.button-italic').click(function() {
        document.execCommand('italic');
    });

    $('.button-underline').click(function() {
        document.execCommand('underline');
    });

    // Save indicator

    var isSaved = true;
    var isSaving = false;
    var infoEditorTitle = $('.info-editor-title')

    function notSaved() {
        if (isSaved) {
            isSaved = false;
            changeSaveIndicator();
        }
    }

    function saved() {
        if (!isSaved) {
            isSaved = true;
            sendBlogSave();
            changeSaveIndicator();
        }
    }

    function changeSaveIndicator() {
        var indicator = infoEditorTitle.next();
        
        if (indicator.hasClass('save-indicator')) {
            indicator.remove();
        }

        if (isSaved) {
            infoEditorTitle.after('<p class="save-indicator saved">Saved</p>');
        } else {
            if (isSaving) {
                infoEditorTitle.after('<p class="save-indicator saving">Saving draft</p>');
            } else {
                infoEditorTitle.after('<p class="save-indicator not-saved">Draft not saved</p>');
            }
        }
    }

    document.addEventListener('input', function() {
        notSaved();
    });

    // Save or publish

    function sendBlogSave() {
        var sections = {};
        var index = 0;

        $('#content-editor').children('.editor').each(function() {

            if (this.classList.contains('editor-p')) {

                section_data = {
                    'id': $(this).children('input[name=section-id]').val(),
                    'content': $(this).find('p.form-content').html(),
                    'type': 'text',
                };

                if (section_data['content'] != '') {
                    sections[index] = section_data;
                }

            } else if (this.classList.contains('editor-image')) {

                section_data = {
                    'id': $(this).children('input[name=section-id]').val(),
                    'content': $(this).find('img.image-content').prop('src'),
                    'caption': $(this).find('p.form-caption').html(),
                    'type': 'image',
                };

                if (section_data['content'] != '') {
                    sections[index] = section_data;
                }

            }
            
            index += 1;
        });

        $.ajax({
            type: 'POST',
            url: '/blog/write/' + $('[name=blog-id]').val(),
            data: {
                'title': $('#content-editor-title').html(),
                'thumbnail': $('.info-form-thumbnail-preview').prop('src'),
                'sections': JSON.stringify(sections),
                'csrfmiddlewaretoken': $('[name=csrfmiddlewaretoken]').val(),
            },
            xhrFields: {
                withCredentials: true,
            },
            success: function(response) {
                if (response != 'Success') {
                    alert('An error occured while saving changes, some changes might not be saved correctly');
                }
            }
        });
    }

    $('.info-form-save').click(function() {
        if (!isSaved) {
            saved();
        }
    });

    $('.info-form-publish').click(function() {
        if (!isSaved) {
            saved();
        }

        var blogId = $('[name=blog-id]').val();

        $.ajax({
            type: 'GET',
            url: '/blog/publish/' + blogId,
            xhrFields: {
                withCredentials: true,
            },
            success: function(response) {
                if (response == 'Success') {
                    window.location = '/blog/' + blogId;
                } else {
                    alert('An error occured while publishing blog, blog might not be published');
                }
            }
        });
    });

    $('.info-form-unpublish').click(function() {
        if (!isSaved) {
            saved();
        }

        var blogId = $('[name=blog-id]').val();

        $.ajax({
            type: 'GET',
            url: '/blog/unpublish/' + blogId,
            xhrFields: {
                withCredentials: true,
            },
            success: function(response) {
                if (response == 'Success') {
                    window.location = '/blog/write/' + blogId;
                } else {
                    alert('An error occured while publishing blog, blog might not be published');
                }
            }
        });
    });
});