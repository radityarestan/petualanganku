$(document).ready(function() {

    var currentUrl = window.location.pathname;
    console.log(currentUrl)

    $('.info-form-label.activity').on('click', function(e){
        e.preventDefault();
        console.log('button pressed!')

        $.ajax({
            url: $(this).attr('action'),
            method: 'get',
            success: function(data){ 
                console.log(data)

                var act_list = data.list_all_act_label[0]
                for(var id in act_list){
                    $('#activity-list').append(
                        `<option class="activity-items" value="` + id + '-' + act_list[id] + `">` + act_list[id] + `</option>`
                    )
                }

                $('#myModal2').css('display', 'block');
                       
                    
            }
        });
    })

    $('.create-activity-label').on('submit', function(e){
        e.preventDefault();
        // console.log('area label created!')
        // console.log($(this).attr('action'))
        // console.log($('.item-province').val())
        
        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: {
                activity : $('.item-activity').val(),
                csrfmiddlewaretoken : $('input[name=csrfmiddlewaretoken]').val()
            },
            success: function(data){
                // console.log('success')
                // console.log(data)
                var act = data.list_activity_label[0]
                
                if (data.message == 'label existed') {
                    alert('Label activity already been selected!')
                } else {
                    $('.list-label.activity-selected').empty()
                    for(var i in act) {
                        console.log(act[i])
                        $('.list-label.activity-selected').append(
                            `<div class="item-container">
                                <li class="activity-items">`
                                + act[i] +
                                `</li>
                                <a class="delete-item-label activity" href="`+ currentUrl + `/delete-activity-label/` + i + `"><input class="delete-btn" type="button" value="&times;"></a>
                            </div>`
                        )
                    }
                    alert('Label activity added!')
                }  
            }
        });

    });


    // event delegation
    $(document).on('click','.delete-item-label.activity', function(e){
        e.preventDefault();
        // console.log('hello')
        var label_soon_delete = $(this).parent()

        $.ajax({
            url: $(this).attr('href'),
            success: function(data){
                // console.log(data)
                var status_delete = data.status_delete
                // console.log(status_delete)
                alert(status_delete)
                label_soon_delete.remove()
            }
        });
        
    });

    $('span.close-modal').click(function(e){
        e.preventDefault();
        // console.log('button pressed!')
        $('#myModal2').css('display', 'none');
    })


});