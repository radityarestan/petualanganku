var modal = document.getElementById("myModal");
var deleteModal = document.getElementById("deleteModal");
var commentToBeDeletedId;
var createForm = false;
var currentBlogId;
var baseCommentId;
var commentToBeEditedId;

function closeModalPopUp() {
    if (modal.style.display == "block") modal.style.display = "none";
    if (deleteModal.style.display == "block") deleteModal.style.display = "none";
}

function createNewComment() {
    var newCommentContent = document.getElementById("create-form").firstChild.value;
    $.ajax({
        type: "POST",
        url: '/blog/create-comment/' + currentBlogId,
        data: {
            'newCommentContent': newCommentContent
        },
        headers: {
            "X-CSRFToken": getCookie("csrftoken")
        },
        success: function(response){
            modal.childNodes[1].childNodes[1].childNodes[3].innerHTML = "Comment successfully created";
            modal.style.display = "block";
            closeCreateForm()
            setTimeout(function () {
                window.location.href = '/blog/' + response.blog_id
            }, 1500);
            
        },
        error: function (response) {
            modal.childNodes[1].childNodes[1].childNodes[3].innerHTML = "Create comment failed";
            modal.style.display = "block";
        }
    });
}

function createReplyComment(callingElement) {
    var replyCommentContent = callingElement.parentElement.parentElement.firstChild.value;
    $.ajax({
        type: "POST",
        url: '/blog/reply-comment/' + baseCommentId,
        data: {
            'newCommentContent': replyCommentContent
        },
        headers: {
            "X-CSRFToken": getCookie("csrftoken")
        },
        success: function(response){
            modal.childNodes[1].childNodes[1].childNodes[3].innerHTML = "Reply successfully created";
            modal.style.display = "block";
            deleteNonCreateForm(callingElement, "reply-form")
            setTimeout(function () {
                window.location.href = '/blog/' + response.blog_id
            }, 1500);
            
        },
        error: function (response) {
            modal.childNodes[1].childNodes[1].childNodes[3].innerHTML = "Create reply failed";
            modal.style.display = "block";
        }
    });
}

function editComment(callingElement) {
    var newCommentContent = callingElement.parentElement.parentElement.firstChild.value;
    $.ajax({
        type: "PUT",
        url: '/blog/edit-comment/' + commentToBeEditedId,
        data: {
            "newCommentContent": newCommentContent
        },
        headers: {
            "X-CSRFToken": getCookie("csrftoken")
        },
        success: function(response){
            modal.childNodes[1].childNodes[1].childNodes[3].innerHTML = "Comment successfully updated";
            deleteNonCreateForm(callingElement, "edit-form")
            modal.style.display = "block";
            setTimeout(function () {
                window.location.href = '/blog/' + response.blog_id
            }, 1500);
        },
        error: function (response) {
            modal.childNodes[1].childNodes[1].childNodes[3].innerHTML = "Comment same as before, please give new comment";
            modal.style.display = "block";
        }
    });
}

function showCreateForm(blogId) {
    currentBlogId = blogId
    if(!createForm) {
        createForm = true
        document.getElementById("header-not-form").insertAdjacentHTML("afterend",
        "<form class='form-comment' id='create-form'>" +
        "<textarea class='textarea-form' placeholder='Write comment here...'></textarea>" +
            "<div class='form-command'>" +
                "<p onclick='closeCreateForm()'>Cancel</p>" +
                "<button type='button' onclick='createNewComment()'>Save</button>" +
            "</div>" +
        "</form>"
        );
    }
    else {
        closeCreateForm()
    }
}

function closeCreateForm() {
    createForm = false
    document.getElementById("create-form").remove()
}

function isThereForm(callingElement, idName) {
    var childs = callingElement.parentElement.parentElement.parentElement.childNodes;
    for (var i = 0; i < childs.length; i++) {
        if (childs[i].nodeType == 1 && childs[i].id == idName) {
            return true
        }
    }
    return false;
}


function showReplyForm(callingElement, commentId) {
    baseCommentId = commentId
    if(!isThereForm(callingElement, "reply-form")) {
        if(isThereForm(callingElement, "edit-form")) deleteNonCreateForm(callingElement, "edit-form")
        callingElement.parentElement.parentElement.insertAdjacentHTML("afterend",
        "<form class='form-comment' id='reply-form'>" +
        "<textarea class='textarea-form' placeholder='Write reply here...'></textarea>" +
            "<div class='form-command'>" +
                "<p onclick='deleteNonCreateForm(this, \"reply-form\")'>Cancel</p>" +
                "<button type='button' onclick='createReplyComment(this)'>Save</button>" +
            "</div>" +
        "</form>"
        ); 
    }
    else deleteNonCreateForm(callingElement, "reply-form")
}

function showEditForm(callingElement, commentId) {
    commentToBeEditedId = commentId
    if(!isThereForm(callingElement, "edit-form")) {
        if(isThereForm(callingElement, "reply-form")) deleteNonCreateForm(callingElement, "reply-form")
        callingElement.parentElement.parentElement.insertAdjacentHTML("afterend",
        "<form class='form-comment' id='edit-form'>" +
        "<textarea class='textarea-form' placeholder='Write new comment here...'></textarea>" +
            "<div class='form-command'>" +
                "<p onclick='deleteNonCreateForm(this, \"edit-form\")'>Cancel</p>" +
                "<button type='button' onclick='editComment(this)'>Save</button>" +
            "</div>" +
        "</form>"
        ); 
    }
    else deleteNonCreateForm(callingElement, "edit-form")
}

function deleteNonCreateForm(callingElement, idName) {
    var childs = callingElement.parentElement.parentElement.parentElement.childNodes;
    for (var i = 0; i < childs.length; i++) {
        if (childs[i].nodeType == 1 && childs[i].id == idName) {
            childs[i].remove()
        }
    }
}

function deleteComment() {
    $.ajax({
        type: "DELETE",
        url: '/blog/delete-comment/' + commentToBeDeletedId,
        headers: {
            "X-CSRFToken": getCookie("csrftoken")
        },
        success: function(response){
            modal.childNodes[1].childNodes[1].childNodes[3].innerHTML = "Comment successfully deleted";
            modal.style.display = "block";
            setTimeout(function () {
                window.location.href = '/blog/' + response.blog_id
            }, 1500);
            
        },
        error: function (response) {
            modal.childNodes[1].childNodes[1].childNodes[3].innerHTML = "Delete comment failed";
            modal.style.display = "block";
        }
    });
    closeModalPopUp()
}

function showDeletePopup(commentId) {
    deleteModal.style.display = "block";
    commentToBeDeletedId = commentId;
}

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}