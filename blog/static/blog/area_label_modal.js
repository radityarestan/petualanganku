$(document).ready(function() {

    var currentUrl = window.location.pathname;
    console.log(currentUrl)

    $('.info-form-label.area').on('click', function(e){
        e.preventDefault();
        // console.log('button pressed!')

        $.ajax({
            url: 'https://kodepos-2d475.firebaseio.com/list_propinsi.json',
            method: 'get',
            success: function(data){ 
                
                $('#myModal').css('display', 'block');

                var province_list = data
                for(var id in province_list){
                    $('#province-list').append(
                        `<option class="province-items" value="` + id + '-' + province_list[id] + `">` + province_list[id] + `</option>`
                    )
                }       
                    
            }
        });
    })

    $('#province-list').on('change', function(e){
        e.preventDefault();
        // console.log('province selected!')
        
        var province_item = $(this).val().split('-')
        // console.log(province_item)

        $.ajax({
            url: 'https://kodepos-2d475.firebaseio.com/list_kotakab/' + province_item[0] + '.json',
            method: 'get',
            
            success: function(data){ 
                var city_list = data
                
                $('#city-list').empty()
                $('#city-list').append(
                    `<option class="-" "value="-" disabled selected>-</option>`
                )

                for(var id in city_list){
                    // console.log(city_list[id])
                    // console.log(id)
                    
                    $('#city-list').append(
                        `<option class="city-items" value="` + id + '-' + city_list[id] + `">` + city_list[id] + `</option>`
                    )
                }       
            }
        });
    })

    $('.create-area-label').on('submit', function(e){
        e.preventDefault();
        // console.log('area label created!')
        // console.log($(this).attr('action'))
        // console.log($('.item-province').val())
        
        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: {
                province : $('.item-province').val(),
                city : $('.item-city').val(),
                csrfmiddlewaretoken : $('input[name=csrfmiddlewaretoken]').val()
            },
            success: function(data){
                // console.log('success')
                // console.log(data)
                var areas = data.list_area_label[0]
                
                if (data.message == 'label existed') {
                    alert('Label area already been selected!')
                } else {
                    $('.list-label.area-selected').empty()
                    for(var i in areas) {
                        // console.log(areas[i])
                        $('.list-label.area-selected').append(
                            `<div class="item-container">
                                <li class="area-items">`
                                + areas[i] +
                                `</li>
                                <a class="delete-item-label" href="`+ currentUrl + `/delete-area-label/` + i + `"><input class="delete-btn" type="button" value="&times;"></a>
                            </div>`
                        )
                    }
                    alert('Label area added!')
                }  
            }
        });

    });


    // event delegation
    $(document).on('click','.delete-item-label.area', function(e){
        e.preventDefault();
        // console.log('hello')
        var label_soon_delete = $(this).parent()

        $.ajax({
            url: $(this).attr('href'),
            success: function(data){
                // console.log(data)
                var status_delete = data.status_delete
                // console.log(status_delete)
                alert(status_delete)
                label_soon_delete.remove()
            }
        });
        
    });

    $('span.close-modal').click(function(e){
        e.preventDefault();
        // console.log('button pressed!')
        $('#myModal').css('display', 'none');
    })


});