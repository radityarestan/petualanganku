import tempfile

from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import resolve

from .views import detail, delete_comment, create_comment, edit_comment, reply_comment
from .models import Blog, Comment, Area

# Create your tests here.

class BlogTest(TestCase):
    def setUp(self):
        # Create mock user
        user = User.objects.create_user('johndoe', 'doe@petualanganku.com', 'JohnDoe123')

        # Create mock blog
        image = tempfile.NamedTemporaryFile(suffix = ".jpg").name
        Blog.objects.create(author=user, title='My Family My Adventure', thumbnail=image)

    def test_blog_has_author(self):
        blog = Blog.objects.first()
        self.assertEqual('johndoe', blog.author.username)

    def test_blog_has_title(self):
        blog = Blog.objects.first()
        self.assertEqual('My Family My Adventure', blog.title)

class AreaTest(TestCase):
    def setUp(self):
        Area.objects.create(province='Jawa Barat', city='Bogor')

    def test_area_has_province(self):
        area = Area.objects.first()
        self.assertEqual('Jawa Barat', area.province)

    def test_area_has_city(self):
        area = Area.objects.first()
        self.assertEqual('Bogor', area.city)

    def test_area_has_no_blog(self):
        area = Area.objects.first()
        self.assertEqual(None, area.blog_labeled)

class CommentTest(TestCase):
    def setUp(self):
        user = User.objects.create_user('kirito', 'kirito@petualanganku.com', 'kiritoasuna')
        user.first_name = 'Kirigaya'
        self.client.login(username = "kirito", password = "kiritoasuna")
        image = tempfile.NamedTemporaryFile(suffix = ".jpg").name
        blog = Blog.objects.create(author = user, title = 'Bali is such a good place', thumbnail=image)
        Comment.objects.create(commentator=user, blog=blog, content="I hope i can get to bali soon")
    
    def test_blog_show_available_comment(self):
        response = self.client.get('/blog/1')
        found = resolve('/blog/1')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(found.func, detail)
        self.assertTemplateUsed(response, 'blog/blog_detail.html')
        html_response = response.content.decode('utf8')
        self.assertIn("I hope i can get to bali soon", html_response)

    def test_comment_model_can_create_new_comment(self):
        comment = Comment.objects.create(
            commentator=User.objects.get(username="kirito"), 
            blog=Blog.objects.get(title="Bali is such a good place"), 
            content="yuhuu"
        )
        comments_count = Comment.objects.all().count()
        self.assertEqual(comments_count, 2)

    def test_delete_comment(self):
        response = self.client.delete('/blog/delete-comment/1', HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        found = resolve('/blog/delete-comment/1')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(found.func, delete_comment)
        comments_count = Comment.objects.all().count()
        self.assertEqual(comments_count, 0)
    
    def test_create_comment(self):
        payload = {
            'newCommentContent': "bali :))"
        }
        response = self.client.post('/blog/create-comment/1', payload, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        found = resolve('/blog/create-comment/1')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(found.func, create_comment)
        comments_count = Comment.objects.all().count()
        self.assertEqual(comments_count, 2)
    
    def test_edit_comment_valid(self):
        payload = b'newCommentContent=bali :))'
        response = self.client.put('/blog/edit-comment/1', payload, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        found = resolve('/blog/edit-comment/1')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(found.func, edit_comment)
        comment = Comment.objects.get(id=1)
        self.assertEqual(comment.content, "bali :))")
    
    def test_edit_comment_invalid(self):
        payload = b'newCommentContent=I hope i can get to bali soon'
        response = self.client.put('/blog/edit-comment/1', payload, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        found = resolve('/blog/edit-comment/1')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(found.func, edit_comment)
        comment = Comment.objects.get(id=1)
        self.assertEqual(comment.content, "I hope i can get to bali soon")
    
    def test_reply_comment(self):
        payload = {
            'newCommentContent': "I hope to"
        }
        response = self.client.post('/blog/reply-comment/1', payload, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        found = resolve('/blog/reply-comment/1')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(found.func, reply_comment)
        comment = Comment.objects.get(id=1)
        comment_replies = Comment.objects.filter(replyTo=comment)
        self.assertEqual(comment_replies[0].content, "I hope to")

