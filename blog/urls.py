from django.urls import path

from . import views

app_name = 'blog'

urlpatterns = [
    # Blog creation
    path('create/', views.create, name='create'),
    path('write/<int:id>', views.write, name='write'),

    # Blog labeling
    path('write/<int:id>/create-area-label', views.create_area_label, name='create_area_label'),
    path('write/<int:id>/delete-area-label/<int:area_id>', views.delete_area_label, name='delete_area_label'),
    path('write/<int:id>/create-activity-label', views.create_activity_label, name='create_activity_label'),
    path('write/<int:id>/delete-activity-label/<int:activity_id>', views.delete_activity_label, name='delete_activity_label'),

    # Blog content editing
    path('section/delete/<int:section_id>', views.delete_blog_section, name='delete_section'),
    path('publish/<int:blog_id>', views.publish_blog, name='publish_blog'),
    path('unpublish/<int:blog_id>', views.unpublish_blog, name='unpublish_blog'),

    # Blog read
    path('<int:blog_id>', views.detail, name='detail_blog'),
    path('list/', views.blog_list, name='blog_list'),

    # Blog comments
    path('create-comment/<int:blog_id>', views.create_comment),
    path('reply-comment/<int:comment_id>', views.reply_comment),
    path('edit-comment/<int:comment_id>', views.edit_comment),
    path('delete-comment/<int:comment_id>', views.delete_comment),

    # Blog reports
    path('report/<int:blog_id>', views.report)
]