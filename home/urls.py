from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.home, name='home'),
    path('about-us', views.about, name='about'),
    path('filter-search', views.filter_search, name='filter_search'),
    path('search-result', views.search_result, name='search_result')
]