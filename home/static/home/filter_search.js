$(document).ready(function() {

    // console.log('hello!')
    // visit filter search page immediately call area api
    call_api_area_province();

    var filter_area_picked = [];
    var filter_activity_picked = [];

    // after province picked, call api area city to get city data correspond to province
    $('#province-list').on('change', function(e){
        e.preventDefault();
        // console.log('province selected!')
        
        var province_item = $(this).val().split('-')
        call_api_area_city(province_item)
    })

    function call_api_area_province() {
        $.ajax({
            url: 'https://kodepos-2d475.firebaseio.com/list_propinsi.json',
            method: 'get',
            success: function(data){ 
                // console.log('api province successfully called!')
                var province_list = data

                for(var id in province_list){

                    // console.log(province_list[id])
                    // console.log(id)

                    $('#province-list').append(
                        `<option class="province-items" value="` + id + '-' + province_list[id] + `">` + province_list[id] + `</option>`
                    )
                }          
            }
        });
    }

    function call_api_area_city(province_item) {
        $.ajax({
            url: 'https://kodepos-2d475.firebaseio.com/list_kotakab/' + province_item[0] + '.json',
            method: 'get',
            
            success: function(data){
                // console.log('api city successfully called!') 
                var city_list = data
                
                $('#city-list').empty()
                $('#city-list').append(
                    `<option class="-" "value="-" disabled selected>-</option>`
                )

                for(var id in city_list){
                    // console.log(city_list[id])
                    // console.log(id)
                    
                    $('#city-list').append(
                        `<option class="city-items" value="` + id + '-' + city_list[id] + `">` + city_list[id] + `</option>`
                    )
                }       
            }
        });
    }


    $('.pick-label.area').on('click', function(e){
        // TODO: PREVENT PICKING THE SAME FILTER
        // TODO: LIMIT FILTER (MAX:3)?

        e.preventDefault();
        // console.log('area label picked!')
        // console.log($('.item-province').val())
        
        var province_item = $('#province-list').val().split('-')
        var city_item = $('#city-list').val().split('-')
        var area = province_item[1] + ', ' + city_item[1]
        
        if (filter_area_picked.includes(area)) {
            alert('Filter area already selected!')
        } else {
            $('#list-area-picked').append(
                `<div class="item-container">
                    <li class="area-items">`
                    + area +
                    `</li>
                    <input type="hidden" class="filter-area" name="filter-area" value="` 
                            + area +
                        `" form="filter-search-form">
                    <input class="remove-filter-btn area" type="button" value="&times;">
                </div>`
            )
            filter_area_picked.push(area)   // add area into array
        }    
        // console.log(filter_area_picked)
    });

    // event delegation
    $(document).on('click','.remove-filter-btn.area', function(e){
        e.preventDefault();
        var filter_soon_delete = $(this).parent()   // select parent html (in this case: item-container)
        var filter_area = $(this).prev()

        filter_area_picked = filter_area_picked.filter(function(e) { return e !== filter_area.val() })   // delete element (with name from ('input.filter-area').val()) from array (filter_area_picked)        
        filter_soon_delete.remove() // remove html element, including itself
        
        // console.log('filter removed!')
        // console.log(filter_area_picked)
    });

    $('.pick-label.activity').on('click', function(e){
        // TODO: PREVENT PICKING THE SAME FILTER
        // TODO: LIMIT FILTER (MAX:3)?

        e.preventDefault();
        // console.log('area label picked!')
        // console.log($('.item-province').val())
        
        var act_item = $('#activity-label-list').val().split('-')
        console.log(act_item[1])
        
        if (filter_activity_picked.includes(act_item[1])) {
            alert('Filter activity already selected!')
        } else {
            $('#list-act-picked').append(
                `<div class="item-container">
                    <li class="activity-items">`
                        + act_item[1] +
                    `</li>
                    <input type="hidden" class="filter-activity" name="filter-activity" value="` 
                        + act_item[1] +
                    `" form="filter-search-form">
                    <input class="remove-filter-btn activity" type="button" value="&times;">
                </div>`
            )
            filter_activity_picked.push(act_item[1])   // add area into array
        }    
        // console.log(filter_act_picked)
    });

    // event delegation
    $(document).on('click','.remove-filter-btn.activity', function(e){
        e.preventDefault();
        var filter_soon_delete = $(this).parent()   // select parent html (in this case: item-container)
        var filter_act = $(this).prev()


        filter_activity_picked = filter_activity_picked.filter(function(e) { return e !== filter_act.val() })   // delete element (with name from ('input.filter-area').val()) from array (filter_area_picked)        
        filter_soon_delete.remove() // remove html element, including itself
        
        // console.log('filter removed!')
        // console.log(filter_area_picked)
    });
    
});

