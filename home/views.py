from django.http.response import HttpResponse
from blog.models import Blog, Area
from labels.models import ActivityLabel
from django.db.models import Q
from django.shortcuts import render
# from django.contrib.postgres.search import SearchVector, SearchQuery, SearchRank
# Create your views here.

def home(request):
    context = {'list_blog_all': Blog.objects.filter(is_published=True).all()}
    return render(request, "home/index.html", context=context)

def about(request):
    return render(request, "home/about_us.html")

def filter_search(request):
    list_act_all = ActivityLabel.objects.all()
    context = {'list_act_all': list_act_all}
    return render(request, "home/filter_search.html", context=context)

def search_result(request):
    if request.method == "POST":
        if request.POST.get('keyword-search'):
            keyword = request.POST.get('keyword-search')
            keyword_lookup = Q(title__icontains=keyword) & Q(is_published=True)
            
            full_lookup = keyword_lookup

            if request.POST.getlist('filter-area'):
                filter_area_lookup = None
                filter_area = request.POST.getlist('filter-area')

                for i in filter_area:
                    area = i.split(', ')
                    if i == filter_area[0]:

                        filter_area_lookup = Q(
                            blog_area_label__province=area[0],
                            blog_area_label__city=area[1],
                            )

                    else:
                        filter_area_lookup |= Q(
                            blog_area_label__province=area[0],
                            blog_area_label__city=area[1],
                            )
                if filter_area_lookup:
                    full_lookup &= filter_area_lookup
            
            if request.POST.getlist('filter-activity'):
                filter_activity_lookup = None
                filter_activity = request.POST.getlist('filter_activity')

                for i in filter_activity:
                    if i == filter_activity[0]:

                        filter_activity_lookup = Q(
                            activity_labels__name=i,
                            )

                    else:
                        filter_activity_lookup |= Q(
                            activity_labels__name=i,
                            )

                if filter_activity_lookup:
                    full_lookup &= filter_activity_lookup

            search_result = Blog.objects.filter(full_lookup)
            context = {'blogs' : search_result}
        else:
            return HttpResponse('Please enter keyword for search.')
    return render(request, "home/search_result.html", context)