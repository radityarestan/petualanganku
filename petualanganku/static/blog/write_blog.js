$(document).ready(function() {

    // Utility

    function format(fmt, ...args) {
        if (!fmt.match(/^(?:(?:(?:[^{}]|(?:\{\{)|(?:\}\}))+)|(?:\{[0-9]+\}))+$/)) {
            throw new Error('invalid format string.');
        }
        return fmt.replace(/((?:[^{}]|(?:\{\{)|(?:\}\}))+)|(?:\{([0-9]+)\})/g, (m, str, index) => {
            if (str) {
                return str.replace(/(?:{{)|(?:}})/g, m => m[0]);
            } else {
                if (index >= args.length) {
                    throw new Error('argument index is out of range in format');
                }
                return args[index];
            }
        });
    }

    // Title sync

    var titleForm = document.getElementById('info-form-title');
    var titleEditor = $('#content-editor-title');

    document.getElementById('content-editor-title').addEventListener('input', function() {
        titleForm.value = titleEditor.text();
    }, false);

    // Save indicator

    var isSaved = true;
    var isSaving = false;
    var infoEditorTitle = $('.info-editor-title')

    function notSaved() {
        if (isSaved) {
            isSaved = false;
            changeSaveIndicator();
        }
    }

    function saved() {
        if (!isSaved) {
            isSaved = true;
            changeSaveIndicator();
        }
    }

    function changeSaveIndicator() {
        var indicator = infoEditorTitle.next();
        
        if (indicator.hasClass('save-indicator')) {
            indicator.remove();
        }

        if (isSaved) {
            infoEditorTitle.after('<p class="save-indicator saved">Saved</p>');
        } else {
            if (isSaving) {
                infoEditorTitle.after('<p class="save-indicator saving">Saving draft</p>');
            } else {
                infoEditorTitle.after('<p class="save-indicator not-saved">Draft not saved</p>');
            }
        }
    }

    document.addEventListener('input', function() {
        notSaved();
    });

    // Thumbnail update

    var THUMBNAIL_PREVIEW = `
    <img class="info-form-thumbnail-preview" src="{0}"/>
    `

    var thumbnail = $('.info-form-image-input');

    $('#info-form-thumbnail').on('change', function(e) {
        var url = URL.createObjectURL(e.target.files[0]);
        thumbnail.html(format(THUMBNAIL_PREVIEW, url));
    });

    // Add and remove section

    var BLANK_TEXT_FORM = `
    <div class="editor editor-p">
        <button class="button-remove-section">
            <i class="fa fa-minus"></i>
        </button>
        <p class="form-text form-content" contenteditable="true" placeholder="Write something..." spellcheck="false"></p>
    </div>
    `

    var IMAGE_SECTION = `
    <div class="editor editor-image">
        <button class="button-remove-section">
            <i class="fa fa-minus"></i>
        </button>
        <img class="image-content" src="{0}"/>
        <p class="form-text form-caption" contenteditable="true" placeholder="Image caption (optional)" spellcheck="false"></p>
    </div>
    `

    $('button.button-add-section-text').click(function() {
        $(this).parent().before(BLANK_TEXT_FORM)
    });

    $('#form-image').on('change', function(e) {
        var url = URL.createObjectURL(e.target.files[0]);
        $(this).parent().before(format(IMAGE_SECTION, url));
        $(this).val('');
    });

    $('#content-editor').on('click', '.button-remove-section', function(e) {
        notSaved();
        $(this).parent().remove();
    });

    document.getElementById('content-editor').addEventListener('keypress', (e) => {
        if (e.keyCode == 13) {
            e.preventDefault();
        }
    });

    function setEndOfContenteditable(contentEditableElement) {
        // Source: https://stackoverflow.com/a/3866442
        var range,selection;

        if(document.createRange) {
            range = document.createRange();
            range.selectNodeContents(contentEditableElement);
            range.collapse(false);
            selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);
        } else if(document.selection) { 
            range = document.body.createTextRange();
            range.moveToElementText(contentEditableElement);
            range.collapse(false);
            range.select();
        }
    }

    $(document).on('keyup', '.form-text', function(e) {
        // Backspace
        if (e.keyCode == 8) {
            if ($(this).is(':empty') && $(this).hasClass('form-content')) {
                var prev = $(this).parent().prev().find('.form-text');
                setEndOfContenteditable(prev.get(0));

                $(this).parent().remove();
            }
        }
        // Enter
        else if (e.keyCode == 13) {
            $(this).parent().after(BLANK_TEXT_FORM);
            var next = $(this).parent().next().find('.form-text');
            next.focus();
        }
    });

    // Change selected status for editor

    var prevButton = null;

    $('#content-editor').on('focus', '.editor', function(e) {
        if (prevButton != null) {
            prevButton.css('visibility', 'hidden');
        }

        prevButton = $(this).find('.button-remove-section');
        prevButton.css('visibility', 'visible');
        $(this).addClass('editor-focus');
    });

    $('#content-editor').on('blur', '.editor', function(e) {
        $(this).removeClass('editor-focus');
    });

    // Text editing

    var getSelected = function() {
        var t = '';

        if (window.getSelection) {
            t = window.getSelection();
        } else if (document.getSelection) {
            t = document.getSelection();
        } else if (document.selection) {
            t = document.selection.createRange().text;
        }
        
        return t;
    }

    var toolsWidth = $('#tools').width();

    $(document).bind('mouseup', function() {
        var selectedText = getSelected();
        var bound = selectedText.getRangeAt(0).getBoundingClientRect();

        if(selectedText != ''){
            if (selectedText.anchorNode.parentElement.classList.contains('form-text')) {
                $('#tools').css({
                    'left': (bound['left'] + bound['right']) / 2 - toolsWidth / 2,
                    'top' : bound['top'] - 40,
                }).fadeIn(200);
            }
        } else {
            $('#tools').fadeOut(200);
        }
    });

    $('.button-bold').click(function() {
        document.execCommand('bold');
    });

    $('.button-italic').click(function() {
        document.execCommand('italic');
    });

    $('.button-underline').click(function() {
        document.execCommand('underline');
    });

    // Save or publish

    // TODO: complete section implement
    async function sendBlogSave() {
        console.log('called');
        var sections = [];

        $('#content-editor').children('.editor').each(function() {
            if (this.classList.contains('editor-title')) {
                console.log('found title');
            } else if (this.classList.contains('editor-p')) {
                console.log('found text');
            } else if (this.classList.contains('editor-image')) {
                console.log('found image');
            }
        });

        var param = {
            'title': $('#info-form-title').val(),
            'thumbnail': $('#info-form-thumbnail').val(),
            'section': sections,
        };

        return param;
    }

    $('.info-form-save').click(function() {
        if (!isSaved) {
            sendBlogSave();
            saved();
        }
    });

    $('.info-form-publish').click(function() {
        if (!isSaved) {
            sendBlogSave();
            saved();

            // TODO: send blog publish
        }
    });
});