var modal = document.getElementById("myModal");
var deleteModal = document.getElementById("deleteModal");
var elementToBeDelete;
createForm = false

function closeModalPopUp() {
    if (modal.style.display == "block") modal.style.display = "none";
    if (deleteModal.style.display == "block") deleteModal.style.display = "none";
}

function createNewComment() {
    var newCommentContent = document.getElementById("create-form").firstChild.value;
    childNodesLength = document.getElementById("comment-comment").childNodes.length
    var adjacentNode = document.getElementById("comment-comment").childNodes[childNodesLength-2]
    adjacentNode.insertAdjacentHTML("afterend",
        "<div class='a-comment'>" +
            "<div class='comment-top'>" +
                "<div class = 'top-image'>" +
                    "<img src='static/blog/blog_detail/images/kirito.jpg'>" +
                "</div>" +
                "<div class = 'no-image-top'>" +
                    "<h4>Nama penulis yang login</h4>" +
                    "<p>1 Desember 2021 22:00</p>" +
                "</div>" +
            "</div>" +
            "<div class='comment-body'>" +
                `<p>${newCommentContent}</p>` +
            "</div>" +
            "<div class='comment-command'>" +
                "<div class='left-command'>" +
                    "<p onclick='showEditForm(this)'>Edit</p>" +
                    "<p onclick='showDeletePopup(this)'>Delete</p>" +
                "</div>" +
                "<div class='right-command'>" +
                    "<p onclick='showReplyForm(this)'>Reply</p>" +
                "</div>" +
            "</div>" +
        "</div>"
    );
    modal.childNodes[1].childNodes[1].childNodes[3].innerHTML = "Comment successfully created";
    modal.style.display = "block";
    closeCreateForm()
}

function createReplyComment(callingElement) {
    var replyCommentContent = callingElement.parentElement.parentElement.firstChild.value;
    childNodesLength = callingElement.parentElement.parentElement.parentElement.parentElement.childNodes.length;
    var adjacentNode = callingElement.parentElement.parentElement.parentElement.parentElement.childNodes[childNodesLength-2];
    adjacentNode.insertAdjacentHTML("afterend",
    "<div class='reply-comment'>" +
        "<div class='comment-top'>" +
            "<div class = 'top-image'>" +
                "<img src='static/blog/blog_detail/images/kirito.jpg'>" +
            "</div>" +
            "<div class = 'no-image-top'>" +
                "<h4>Nama penulis yang login</h4>" +
                "<p>1 Desember 2021 22:00</p>" +
            "</div>" +
        "</div>" +
        "<div class='comment-body'>" +
            `<p>${replyCommentContent}</p>` +
        "</div>" +
        "<div class='comment-command'>" +
            "<div class='left-command'>" +
                "<p onclick='showEditForm(this)'>Edit</p>" +
                "<p onclick='showDeletePopup(this)'>Delete</p>" +
            "</div>" +
            "<div class='right-command'>" +
                "<p onclick='showReplyForm(this)'>Reply</p>" +
            "</div>" +
        "</div>" +
    "</div>"
    );
    modal.childNodes[1].childNodes[1].childNodes[3].innerHTML = "Reply successfully created";
    modal.style.display = "block";
    deleteNonCreateForm(callingElement, "reply-form")
}

function editComment(callingElement) {
    var newCommentContent = callingElement.parentElement.parentElement.firstChild.value;
    var childs = callingElement.parentElement.parentElement.parentElement.childNodes;
    var isValidate = false;
    for (var i = 0; i < childs.length; i++) {
        if (childs[i].nodeType == 1 && childs[i].className == "comment-body") {
            pNodeToBeChanged = callingElement.parentElement.parentElement.parentElement.childNodes[i].childNodes[1];
            if (pNodeToBeChanged.innerHTML != newCommentContent) {
                isValidate = true;
            }
            break;
        }
    }
    if(isValidate) {
        pNodeToBeChanged.innerHTML = newCommentContent;
        modal.childNodes[1].childNodes[1].childNodes[3].innerHTML = "Comment successfully updated";
        deleteNonCreateForm(callingElement, "edit-form")
    }
    else {
        modal.childNodes[1].childNodes[1].childNodes[3].innerHTML = "Comment same as before, please give new comment";
    }
    modal.style.display = "block";
}

function showCreateForm() {
    if(!createForm) {
        createForm = true
        document.getElementById("header-not-form").insertAdjacentHTML("afterend",
        "<form class='form-comment' id='create-form'>" +
        "<textarea class='textarea-form' placeholder='Write comment here...'></textarea>" +
            "<div class='form-command'>" +
                "<p onclick='closeCreateForm()'>Cancel</p>" +
                "<button type='button' onclick='createNewComment()'>Save</button>" +
            "</div>" +
        "</form>"
        );
    }
    else {
        closeCreateForm()
    }
}

function closeCreateForm() {
    createForm = false
    document.getElementById("create-form").remove()
}

function isThereForm(callingElement, idName) {
    var childs = callingElement.parentElement.parentElement.parentElement.childNodes;
    for (var i = 0; i < childs.length; i++) {
        if (childs[i].nodeType == 1 && childs[i].id == idName) {
            return true
        }
    }
    return false;
}

function showReplyForm(callingElement) {
    if(!isThereForm(callingElement, "reply-form")) {
        if(isThereForm(callingElement, "edit-form")) deleteNonCreateForm(callingElement, "edit-form")
        callingElement.parentElement.parentElement.insertAdjacentHTML("afterend",
        "<form class='form-comment' id='reply-form'>" +
        "<textarea class='textarea-form' placeholder='Write reply here...'></textarea>" +
            "<div class='form-command'>" +
                "<p onclick='deleteNonCreateForm(this, \"reply-form\")'>Cancel</p>" +
                "<button type='button' onclick='createReplyComment(this)'>Save</button>" +
            "</div>" +
        "</form>"
        ); 
    }
    else deleteNonCreateForm(callingElement, "reply-form")
}

function showEditForm(callingElement) {
    if(!isThereForm(callingElement, "edit-form")) {
        if(isThereForm(callingElement, "reply-form")) deleteNonCreateForm(callingElement, "reply-form")
        callingElement.parentElement.parentElement.insertAdjacentHTML("afterend",
        "<form class='form-comment' id='edit-form'>" +
        "<textarea class='textarea-form' placeholder='Write new comment here...'></textarea>" +
            "<div class='form-command'>" +
                "<p onclick='deleteNonCreateForm(this, \"edit-form\")'>Cancel</p>" +
                "<button type='button' onclick='editComment(this)'>Save</button>" +
            "</div>" +
        "</form>"
        ); 
    }
    else deleteNonCreateForm(callingElement, "edit-form")
}

function deleteNonCreateForm(callingElement, idName) {
    var childs = callingElement.parentElement.parentElement.parentElement.childNodes;
    for (var i = 0; i < childs.length; i++) {
        if (childs[i].nodeType == 1 && childs[i].id == idName) {
            childs[i].remove()
        }
    }
}

function deleteComment() {
    elementToBeDelete.parentElement.parentElement.parentElement.remove();
    closeModalPopUp()
}

function showDeletePopup(callingElement) {
    deleteModal.style.display = "block";
    elementToBeDelete = callingElement;
}