import json
import random
import string

from django.shortcuts import render, redirect
from django.contrib.auth import login as auth_login, logout as auth_logout
from django.contrib.auth.models import User
from django.http import HttpResponse
from .models import *
from requests import request

# Create your views here.

def login(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        token = request.POST.get('token')
        username = request.POST.get('username')

        firebase_response = json.loads(check_token(token))

        if 'users' not in firebase_response:
            # TODO: Bad request
            return HttpResponse('Error')
        
        user = firebase_response['users']

        if user == 0:
            # TODO: No user found
            return HttpResponse('Error')

        user = user[0]

        if user['email'] != email:
            # TODO: Email doesnt match
            return HttpResponse('Error')

        if user['emailVerified'] == 'False' or not user['emailVerified']:
            # TODO: Email not verified
            return HTtpResponse('Error')

        response = proceed_login(email, username)
        auth_login(request, response)
        return HttpResponse('Success')

    else:
        return render(request, 'authentication/index.html')

def check_token(token):
    url = 'https://identitytoolkit.googleapis.com/v1/accounts:lookup'

    data = 'key=AIzaSyAVFXwez3wHHySedADXMIF-9rODXgPT3wk&idToken=' + token
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}

    response = request('POST', url, headers=headers, data=data)
    return response.text

def proceed_login(email, username):
    # Check if user with email exists
    if User.objects.filter(email=email).exists():
        user = User.objects.get(email=email)

    else:
        # Generate random password, note that user can't login using this password
        password = ''.join(random.choices(string.ascii_letters + string.digits + "!@#$%^&*()-_=+,.", k=32))

        # Create user instance
        user = User.objects.create_user(username=username,email=email,password=password)
        profile = Profile(user=user, is_admin=False)
    # backend = 'django.contrib.auth.backends.ModelBackend'
    return user

def logout(request):
    auth_logout(request)
    return redirect('/')
