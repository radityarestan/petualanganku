// Firebase configurations
const firebaseConfig = {
    apiKey: "AIzaSyAVFXwez3wHHySedADXMIF-9rODXgPT3wk",
    authDomain: "petualanganku-91d25.firebaseapp.com",
    projectId: "petualanganku-91d25",
    storageBucket: "petualanganku-91d25.appspot.com",
    messagingSenderId: "447150539056",
    appId: "1:447150539056:web:cbffb7baa218d90ea308f5"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// Send login details
function sendDataToServer(email, token, username) {
    // TODO: handle username dan displayname

    $.ajax({
        type: 'POST',
        url: '/login/',
        data: {
            'email': email,
            'username': username,
            'token': token,
            // 'displayname': displayname,
            'csrfmiddlewaretoken': $('[name=csrfmiddlewaretoken]').val(),
        },
        xhrFields: {
            withCredentials: true,
        },
        success: function(response) {
            console.log(response);

            if (response == "Success") {
                // Send login success or redirect
                alert('Login Successful');
            }
            else {
                // Send error notification
                alert('An Error Occured');
            }
        }
    });
}

// Process response
function processAuthenticationResponse(response) {
    var userData = response.user;
    var email = userData.email;
    var token = userData.xa;

    if (token != null && token != undefined && token != '') {
        // Send login data
        sendDataToServer(email, token, email);
    }
}

// Send verification email
function sendEmailVerification() {
    firebase.auth().currentUser.sendEmailVerification().then(function(response) {
        console.log(response);
        // Send notification verification email sent
    }).catch(function(error) {
        console.log(error);
        // Send notification error occured
    });
}

// Signup with email
var signupEmail = $('#signup-email');
var signupPassword = $('#signup-pass');
var signupConfirmPassword = $('#signup-confirm');

$('#signup-btn').click(function() {
    // TODO: disable button temporarily, enable again if error
    // TODO: give loading indication

    var email = signupEmail.val();
    var pass = signupPassword.val();
    var confirm = signupConfirmPassword.val();

    if (pass == confirm) {
        firebase.auth().createUserWithEmailAndPassword(email, pass).then(function(response) {

            console.log(response);
            sendEmailVerification();
            // TODO: reenable button

        });
    } else {
        // TODO: password and confirm password doesn't match
        signupPassword.val('');
        signupConfirmPassword.val('');
    }
});

// Login with email
var loginEmail = $('#login-email');
var loginPassword = $('#login-pass');

$('#login-button').click(function() {
    // TODO: disable button temporarily, enable again if error
    // TODO: give loading indication

    var email = loginEmail.val();
    var pass = loginPassword.val();

    firebase.auth().signInWithEmailAndPassword(email, pass).then(function(response) {
        
        console.log(response);
        processAuthenticationResponse(response);

    }).catch(function(error) {
        console.log(error);
    });
});

// Open sign in popup
function openSignInPopup(provider) {
    firebase.auth().signInWithPopup(provider).then(function(response) {

        console.log(response);
        processAuthenticationResponse(response);

    }).catch(function(error) {
        console.log(error);
    });
}

// Login with Google
$('.login-google').click(function() {
    var provider = new firebase.auth.GoogleAuthProvider();
    openSignInPopup(provider);
});

// Login with Facebook
$('.login-facebook').click(function() {
    var provider = new firebase.auth.FacebookAuthProvider();
    openSignInPopup(provider);
});

// Login with Twitter
$('.login-twitter').click(function() {
    var provider = new firebase.auth.TwitterAuthProvider();
    openSignInPopup(provider);
});
