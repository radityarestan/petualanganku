from django.urls import path

from . import views

app_name = 'manage_report'

urlpatterns = [
    path('', views.reports, name='reports'),
    path('report-detail/<int:report_id>', views.report_detail, name = 'report_detail'),
    path('report-detail/accepted-form/<int:report_id>', views.accepted_form, name = 'accepted_form_report'),
    path('reject-report/<int:report_id>', views.reject_report, name = 'reject_report'),
    path('accept-report/<int:report_id>', views.accept_report,  name = 'accept_report'),
]