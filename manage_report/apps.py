from django.apps import AppConfig


class ManageReportConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'manage_report'
