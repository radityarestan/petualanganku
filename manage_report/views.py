from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
import threading

from blog.models import Blog, Report

toast_message = ''
def update_toast_message(message) :
    global toast_message
    toast_message = message

def reports(request):
    all_reports = Report.objects.all()
    response = {'report_list': all_reports, 'toast_message': toast_message}
    threading.Timer(2, refreshing_toast_message).start()
    return render(request, 'manage_report/reports.html', response)

def report_detail(request, report_id):
    report_detail = Report.objects.get(id=report_id)
    report_detail = {
        'report_id': report_id,
        'report_type': report_detail.reportType,
        'report_description': report_detail.reportDescription,
        'report_created_at': report_detail.created_at,
        'blog_title': report_detail.blog.title,
        'blog_author': report_detail.blog.author,
        'blog_id': report_detail.blog.id,
        'similar_reports': Report.objects.filter(blog__id=report_detail.blog.id),
    }
    return render(request, 'manage_report/report-detail.html', report_detail)

def accepted_form(request, report_id):
    return render(request, 'manage_report/accepted-form.html', {'report_id': report_id})

def accept_report(request, report_id):
    update_toast_message('Report has been accepted')
    # delete blog dan delete all report related to that blog 
    report = Report.objects.get(id=report_id)
    Report.objects.filter(blog__id=report.blog.id).delete()
    blog = Blog.objects.get(id=report.blog.id)
    blog.delete()
    messages = request.POST['messages']
    # send messages to the author
    return HttpResponseRedirect('/manage-report/')

def reject_report(request, report_id):
    update_toast_message('Report has been rejected')
    deleted_report = Report.objects.get(id=report_id)
    deleted_report.delete()
    return HttpResponseRedirect('/manage-report/')

def refreshing_toast_message():
    update_toast_message('')